#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>
class book{
private:
    bool exist=true;
    int punishment=0;
    time_t getting, putting;
public:
    std::string Title,Author,Genre,Height,Publisher;
    book(){}
    void t(std::string v){Title=v;}
    void a(std::string v){Author=v;}
    void g(std::string v){Genre=v;}
    void h(std::string v){Height=v;}
    void p(std::string v){Publisher=v;}
    void tim(){
        if(!exist){
            time_t t=time(NULL);
        if(t-getting>3600){
            if ((t-getting)%60!=0){
                if ((t-getting)%60%60!=0){
                    std::cout<<(t-getting)%60%60-1<<" hours overdue"<<std::endl;
                }
                else{
                    if (t-getting>=3600){
                    std::cout<<(t-getting)%60-60<<" minutes overdue"<<std::endl;
                    }
                }
            }
            else{
                if (t-getting>=3600){
                std::cout<<(t-getting)-3600<<" seconds overdue"<<std::endl;
                }
            }
        }
        else{
            if (60-(t-getting)%60!=0){
                if (1-(t-getting)%60%60!=0){
                    std::cout<<1-(t-getting)%60%60<<" hours left"<<std::endl;
                }
                else{
                    std::cout<<60-(t-getting)%60<<" minutes left"<<std::endl;
                }
            }
            else{
                std::cout<<3600-(t-getting)<<" seconds left"<<std::endl;
            }

        }
        }
    }
    void get(){
    if (exist){
        exist=false;
        std::cout<<Title<<" is taken"<<std::endl;
        getting=time(NULL);
    }
    else {std::cout<<Title<<" is out of stock"<<std::endl;}
    }
    void put(){
        if (!exist){
            exist=true;
            std::cout<<Title<<" has been returned"<<std::endl;
            putting=time(NULL);
            if(putting - getting-3600>0){
                punishment +=1000;
                std::cout<<"please, pay "<<punishment<<" dollars"<<std::endl;
            }

        }
        else{std::cout<<"You don't have this book"<<std::endl;

    }
    }
};
